package com.example.githubsearch

import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.core.requests.CancellableRequest
import com.github.kittinunf.fuel.serialization.responseObject
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json

@ImplicitReflectionSerializer
class GHNetwork {

    companion object {

        fun searсhUsers(
            text: String,
            handler: (Result<SearchUsersResponse, FuelError>) -> Unit
        ): CancellableRequest {

            val stringUrl = "https://api.github.com/search/users?q=$text"
            val task = stringUrl
                .httpGet()
                .responseObject<SearchUsersResponse>(json = Json.nonstrict) { _, response, result ->

                    handler(result)

                }
            task.join()
            return task
        }


    }
}
