package com.example.githubsearch

import kotlinx.serialization.Serializable
import java.net.URI

@Serializable
data class SearchUser(val login: String, val avatar_url: String, val score: Double) {

}