package com.example.githubsearch

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class UserView(context: Context) : LinearLayout(context) {

    var userIcon : ImageView? = null
    var userLogin : TextView? = null
    var userScore : TextView? = null

    init {

        inflate(context, R.layout.user_view, this)
        userIcon = findViewById(R.id.avatar)
        userLogin = findViewById(R.id.login)
        userScore = findViewById(R.id.score)

    }

}