package com.example.githubsearch

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.BatchingListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.core.requests.CancellableRequest
import com.github.kittinunf.result.Result
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.user_view.view.*
import kotlinx.serialization.ImplicitReflectionSerializer


@ImplicitReflectionSerializer
class UserListAdapter () : RecyclerView.Adapter<UserListAdapter.MyViewHolder>() {


    private var accounts: ArrayList<SearchUser> = ArrayList()

    private var searchRequest: CancellableRequest? = null

    fun search(propertyOfSearch: String) {

        this.searchRequest?.cancel()
        this.searchRequest = GHNetwork.Companion.searсhUsers(propertyOfSearch) { result ->
            when (result) {
                is Result.Success -> {
                    val searchResponse = result.get()
                    this.accounts = searchResponse.items
                    this.notifyDataSetChanged()
                }
                is Result.Failure -> {
                    result.getException()
//                    MainActivity().showToast()
                }
            }
        }
    }

    class MyViewHolder(userView: UserView) :
        RecyclerView.ViewHolder(userView) {

        private var accountView = userView
        private var account: SearchUser? = null
        fun updateWith(account: SearchUser) {

            this.account = account
            Picasso.get()
                .load(account.avatar_url)
                .fit()
                .into(accountView.avatar)

            accountView.login.text = account.login
            accountView.score.text = account.score.toString()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = UserView(parent.context)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.updateWith(accounts[position])
    }

    override fun getItemCount(): Int {
        return accounts.count()
    }
}

