package com.example.githubsearch
import kotlinx.serialization.Serializable

@Serializable
data class SearchUsersResponse(val total_count: Int, val incomplete_results: Boolean, val items:ArrayList<SearchUser>) {

}