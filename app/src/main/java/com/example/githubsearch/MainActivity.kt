package com.example.githubsearch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.view.isEmpty
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.ImplicitReflectionSerializer
import java.util.*
import kotlin.concurrent.timerTask
import com.example.githubsearch.UserListAdapter as UserListAdapter

@ImplicitReflectionSerializer
open class MainActivity : AppCompatActivity() {

    var userListAdapter: UserListAdapter? = null

    private var timer: Timer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerContainerView.apply {
            this.layoutManager = llm
            this.adapter = UserListAdapter()
        }

        textWatcher()
    }


    private fun textWatcher() {

        searchText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                timer.cancel()
                timer = Timer()
                timer.schedule(
                    timerTask {
                        if (recyclerContainerView.isEmpty()) {
                            (recyclerContainerView.adapter as? UserListAdapter)?.search(searchText.text.toString())
                        } else {
//                            recyclerContainerView.removeAllViewsInLayout()
                            (recyclerContainerView.adapter as? UserListAdapter)?.search(searchText.text.toString())
                        }
                    }, 500
                )
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

            }
        })

    }

    fun showToast() {
        Toast.makeText(
            this@MainActivity,
            "Error, something wrong :)", //Не будет работать при первом запросе
            Toast.LENGTH_SHORT
        ).show()
    }
}

